import express, { Response } from 'express';
import helmet from 'helmet';
import mongoSanitize from 'express-mongo-sanitize';
import cors from 'cors';
import compression from 'compression';
import responseTime from 'response-time';
import passport from 'passport';
import routes from '@routes/v1';
import * as morgan from '@config/morgan';
import configEnv from '@config/configEnv';
import jwtStrategy from '@config/passport';
import { notfound } from './utils/apiErrorInput';

const app = express();

if (configEnv.env !== 'test') {
  app.use(morgan.successHandler);
  app.use(morgan.errorHandler);
}
app.use(responseTime());
// set security HTTP headers
app.use(helmet());

// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// sanitize request data
app.use(mongoSanitize());

// gzip compression
app.use(compression({ level: 1 }));

// enable cors
app.use(cors());
app.options('*', cors());

// jwt authentication
app.use(passport.initialize());
passport.use('jwt', jwtStrategy);

// v1 api routes
app.use('/v1', routes);

// send back a 404 error for any unknown api request
app.use((_req, _res: Response, next) => {
  next(_res.json(notfound()));
});

export default app;
