import dotenv from 'dotenv';
import Joi, { ValidationResult } from 'joi';
import path from 'path';

dotenv.config({ path: path.join(__dirname, '../../.env') });

type envApp = 'test' | 'production' | 'development';
type configApp = {
  port: string;
  env: envApp;
  mongoose: {
    url: string;
    options: {
      useCreateIndex: boolean;
      useNewUrlParser: boolean;
      useUnifiedTopology: boolean;
    };
  };
  jwt: {
    secret: string;
    accessExpirationMinutes: number;
    refreshExpirationDays: number;
    resetPasswordExpirationMinutes: number;
    verifyEmailExpirationMinutes: number;
  };
};

interface IEnvConfigResult extends ValidationResult {
  value: {
    NODE_ENV: envApp;
    PORT: number;
    MONGODB_URL: string;
    JWT_SECRET: string;
    JWT_ACCESS_EXPIRATION_MINUTES: number;
    JWT_REFRESH_EXPIRATION_DAYS: number;
    JWT_RESET_PASSWORD_EXPIRATION_MINUTES: number;
    JWT_VERIFY_EMAIL_EXPIRATION_MINUTES: number;
  };
}

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi.string()
      .valid('production', 'development', 'test')
      .required(),
    PORT: Joi.number().default(process.env.PORT),
    MONGODB_URL: Joi.string()
      .required()
      .default(process.env.MONGODB_URL)
      .description('Mongo DB url'),
    JWT_SECRET: Joi.string()
      .required()
      .default(process.env.JWT_SECRET)
      .description('JWT secret key'),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number()
      .default(60)
      .description('minutes after which access tokens expire'),
    JWT_REFRESH_EXPIRATION_DAYS: Joi.number()
      .default(60)
      .description('days after which refresh tokens expire'),
    JWT_RESET_PASSWORD_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description(
        'minutes after which reset password token expires'
      ),
    JWT_VERIFY_EMAIL_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description('minutes after which verify email token expires'),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema
  .prefs({ errors: { label: 'key' } })
  .validate(process.env) as IEnvConfigResult;

if (error) {
  throw new Error(`Config validation error: ${error.message}`);
}
const configEnv: configApp = {
  port: process.env.PORT,
  env: envVars.NODE_ENV,
  mongoose: {
    url: envVars.MONGODB_URL,
    options: {
      useCreateIndex: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    },
  },
  jwt: {
    secret: envVars.JWT_SECRET,
    accessExpirationMinutes: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    refreshExpirationDays: envVars.JWT_REFRESH_EXPIRATION_DAYS,
    resetPasswordExpirationMinutes:
      envVars.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
    verifyEmailExpirationMinutes:
      envVars.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES,
  },
};
export default configEnv;
