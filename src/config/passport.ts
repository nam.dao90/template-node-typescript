import { Strategy, ExtractJwt, StrategyOptions } from 'passport-jwt';
import { VerifyCallback } from 'jsonwebtoken';
import User from '@modules/user/model';
import { tokenTypes } from '@modules/auth/types/model';
import configEnv from './configEnv';

const jwtOptions: StrategyOptions = {
  secretOrKey: configEnv.jwt.secret,
  jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
};

const jwtVerify = async (payload: any, done: VerifyCallback) => {
  try {
    if (payload.type !== tokenTypes.ACCESS) {
      throw new Error('Invalid token type');
    }
    const user = await User.findById(payload.sub).populate('role');
    if (!user) {
      return done(null, null);
    }
    done(null, user);
  } catch (error) {
    done(error, null);
  }
};

const jwtStrategy = new Strategy(jwtOptions, jwtVerify);

export default jwtStrategy;
