import configEnv from '@config/configEnv';
import logger from '@config/logger';
import { Server } from 'http';
import mongoose from 'mongoose';
import app from './app';

let server: Server;

mongoose
  .connect(configEnv.mongoose.url, configEnv.mongoose.options)
  .then(() => {
    logger.info('Connected to MongoDB');
    server = app.listen(configEnv.port, () => {
      logger.info(`Listening to port ${configEnv.port}`);
    });
  })
  .catch(error => logger.error(error));

const exitHandler = () => {
  if (server) {
    server.close(() => {
      logger.info('Server closed');
      process.exit(1);
    });
  } else {
    process.exit(1);
  }
};

const unexpectedErrorHandler = (error: Error) => {
  logger.error(error);
  exitHandler();
};

process.on('uncaughtException', unexpectedErrorHandler);
process.on('unhandledRejection', unexpectedErrorHandler);

process.on('SIGTERM', () => {
  logger.info('SIGTERM received');
  if (server) {
    server.close();
  }
});
