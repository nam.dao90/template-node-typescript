import { Ability } from '@casl/ability';
import permissionServices from '@modules/permission/services';

export type IActionTypes =
  | 'canAdd'
  | 'canView'
  | 'canUpdate'
  | 'canSoftDelete'
  | 'canList';

/** Add others modules into database */
export type IModuleNames =
  | 'users'
  | 'stock'
  | 'orders'
  | 'roles'
  | 'modules';
interface RawRule {
  action: IActionTypes;
  subject: string | string[];
  /** an array of fields to which user has (or not) access */
  fields?: string[];
  /** an object of conditions which restricts the rule scope */
  conditions?: any;
  /** indicates whether rule allows or forbids something */
  inverted?: boolean;
  /** message which explains why rule is forbidden */
  reason?: string;
}
export default class AbilityApp {
  // eslint-disable-next-line no-use-before-define
  static AbilityInstance: AbilityApp = null;

  PermissionRole: Ability | null = null;

  /**
   * @returns {AbilityApp}
   */
  static getInstance() {
    if (AbilityApp.AbilityInstance === null) {
      AbilityApp.AbilityInstance = new AbilityApp();
    }
    return this.AbilityInstance;
  }

  clearPermissionRole() {
    this.PermissionRole = null;
  }

  async getPermissionRole() {
    if (this.PermissionRole === null) {
      const listPermissionRole =
        await permissionServices.getPermissionRole();
      const permissionAllApp: RawRule[] = [];
      listPermissionRole.forEach(permission => {
        const ruleAdd: RawRule = {
          subject: permission.modules.name,
          action: 'canAdd',
          inverted: permission.canAdd === false,
          conditions: { roles: permission.roles.name },
          reason: `Access denied with ${permission.roles.name}`,
        };
        const ruleView: RawRule = {
          subject: permission.modules.name,
          action: 'canView',
          inverted: permission.canView === false,
          conditions: { roles: permission.roles.name },
          reason: `Access denied with ${permission.roles.name}`,
        };
        const ruleUpdate: RawRule = {
          subject: permission.modules.name,
          action: 'canUpdate',
          inverted: permission.canUpdate === false,
          conditions: { roles: permission.roles.name },
          reason: `Access denied with ${permission.roles.name}`,
        };
        const ruleList: RawRule = {
          subject: permission.modules.name,
          action: 'canList',
          inverted: permission.canList === false,
          conditions: { roles: permission.roles.name },
          reason: `Access denied with ${permission.roles.name}`,
        };
        const ruleSoftDelete: RawRule = {
          subject: permission.modules.name,
          action: 'canSoftDelete',
          inverted: permission.canSoftDelete === false,
          conditions: { roles: permission.roles.name },
          reason: `Access denied with ${permission.roles.name}`,
        };
        permissionAllApp.push(
          ruleAdd,
          ruleList,
          ruleSoftDelete,
          ruleUpdate,
          ruleView
        );
      });
      if (permissionAllApp.length > 0) {
        this.PermissionRole = new Ability(permissionAllApp);
      }
    }
    return this.PermissionRole;
  }
}
