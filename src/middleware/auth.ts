import { NextFunction, Request, Response } from 'express';
import passport from 'passport';
import { unauthorizeUser } from '@utils/apiErrorInput';
import { SchemaUser } from '@modules/user/types/model';
import authServices from '@src/modules/auth/services';

const authJwt = (req: Request, res: Response, next: NextFunction) =>
  passport.authenticate(
    'jwt',
    { session: false },
    (err, user: SchemaUser, _failures: any) => {
      if (err || !user) {
        /**
         * if you want custom token failure please code here
         * if(_failures === 'jsonWebExpire')
         */
        return res.json(unauthorizeUser());
      }
      res.locals.user = user;
      next();
    }
  )(req, res, next);

const authToken = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  let token = req.headers.authorization;
  if (!token) {
    return res.json(unauthorizeUser());
  }
  if (/\s/g.test(token)) {
    // eslint-disable-next-line prefer-destructuring
    token = token.split(' ')[1];
  }
  const isExistToken = await authServices.checkExistToken(token);
  if (!isExistToken) {
    return res.json(unauthorizeUser());
  }
  return next();
};
const authMiddleWare = {
  authJwt,
  authToken,
};
export default authMiddleWare;
