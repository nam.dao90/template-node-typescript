import { NextFunction, Request, Response } from 'express';
import { LeanDocument } from 'mongoose';
import { subject } from '@casl/ability';
import { internalException } from '@utils/exception';
import { accessDeniedUser } from '@utils/apiErrorInput';
import { SchemaUser } from '@modules/user/types/model';
import AbilityApp, {
  IActionTypes,
  IModuleNames,
} from './acl/ability';

const accessRoleByModule =
  (role: IActionTypes, modules: IModuleNames) =>
  async (
    _req: Request,
    res: Response & { locals: { user: LeanDocument<SchemaUser> } },
    next: NextFunction
  ) => {
    try {
      const { user } = res.locals;
      const userRoles = {
        roles: user.role[0].name,
      };
      const objUser = subject(modules, userRoles);
      const listRole =
        await AbilityApp.getInstance().getPermissionRole();
      const hasRollAccess = listRole && listRole.can(role, objUser);
      if (!hasRollAccess) return res.json(accessDeniedUser());
      next();
    } catch (err) {
      internalException(res, err);
    }
  };

const accessRolesBase =
  (role: IActionTypes) =>
  async (
    _req: Request,
    res: Response & { locals: { user: LeanDocument<SchemaUser> } },
    next: NextFunction
  ) => {
    try {
      const { user } = res.locals;
      const userRole = {
        name: user.role[0].name,
      };
      // const userRole = user.role.find(
      //   r => r.name === 'admin' || r.name === 'super_admin'
      // );
      // if (!userRole) {
      //   return res.json(accessDeniedUser());
      // }
      if (
        role === 'canSoftDelete' &&
        userRole.name !== 'admin' &&
        userRole.name !== 'super_admin'
      ) {
        return res.json(accessDeniedUser());
      }
      next();
    } catch (err) {
      internalException(res, err);
    }
  };

const roles = {
  accessRoleByModule,
  accessRolesBase,
};

export default roles;
