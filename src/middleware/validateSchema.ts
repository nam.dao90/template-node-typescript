import Joi, { ObjectSchema } from 'joi';
import { Request, Response, NextFunction } from 'express';
import { handleErrorsRequest } from '@utils/apiErrorInput';
import pick from 'lodash/pick';

const validate =
  (schema: { body: ObjectSchema }) =>
  (req: Request, res: Response, next: NextFunction) => {
    const validSchema = pick(schema, ['params', 'query', 'body']);
    const object = pick(req, Object.keys(validSchema));
    const { error } = Joi.compile(validSchema)
      .prefs({ errors: { label: 'key' }, abortEarly: false })
      .validate(object);

    if (error) {
      const dataError = handleErrorsRequest(error);
      return res.json(dataError);
    }
    return next();
  };

export default validate;
