import mongoose from 'mongoose';
import configEnv from '@config/configEnv';
import logger from '@config/logger';
import ModulesModel from '@src/modules/modules/model';
import UserModel from '@modules/user/model';
import TokenModel from '@modules/auth/model';
import PermissionModel from '@modules/permission/model';
import RolesModel from '@modules/roles/model';

const openConnectionDb = () => {
  mongoose
    .connect(configEnv.mongoose.url, configEnv.mongoose.options)
    .then(() => logger.info('connect success'))
    .catch(error => logger.error(error));
};
const clearDataModel = async () => {
  await openConnectionDb();
  await UserModel.deleteMany({}, () =>
    logger.info('clear user model success')
  );
  await TokenModel.deleteMany({}, () =>
    logger.info('clear token model success')
  );
  await PermissionModel.deleteMany({}, () =>
    logger.info('clear permission model success')
  );
  await RolesModel.deleteMany({}, () =>
    logger.info('clear roles model success')
  );
  await ModulesModel.deleteMany({}, () =>
    logger.info('clear modules model success')
  );
  if (mongoose.connection.readyState) {
    await mongoose
      .disconnect()
      .then(() => logger.info('close connect success'))
      .catch(error => logger.error(error));
  }
};

export { clearDataModel };
