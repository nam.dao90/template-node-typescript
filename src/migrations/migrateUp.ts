import bcrypt from 'bcrypt';
import mongoose from 'mongoose';
import configEnv from '@config/configEnv';
import logger from '@config/logger';
/**
 * Model
 */
import ModulesModel from '@modules/modules/model';
import UserModel from '@modules/user/model';
import RolesModel from '@modules/roles/model';
import PermissionModel from '@modules/permission/model';
/**
 * Types
 */
import { SchemaModules } from '@modules/modules/types/model';
import { SchemaRoles } from '@modules/roles/types/model';
import { SchemaPermissionRoles } from '@modules/permission/types/model';
import { SchemaUser } from '@modules/user/types/model';

const createModules = async () => {
  const listModulesBase: Array<SchemaModules> = [
    { name: 'users', aliasName: 'users' },
    { name: 'modules', aliasName: 'modules' },
    { name: 'roles', aliasName: 'roles' },
    { name: 'stock', aliasName: 'stock' },
    { name: 'orders', aliasName: 'orders' },
  ];
  await ModulesModel.insertMany(listModulesBase).then(() =>
    logger.info('create modules model success')
  );
};

const createRoles = async () => {
  const listRoleBase: Array<SchemaRoles> = [
    { name: 'admin', aliasName: 'admin' },
    { name: 'super_admin', aliasName: 'sa' },
    { name: 'sale', aliasName: 'phong-sale' },
    { name: 'member', aliasName: 'thanh vien' },
    { name: 'store', aliasName: 'kho' },
  ];
  await RolesModel.insertMany(listRoleBase).then(() =>
    logger.info('create modules roles success')
  );
};

const createPermissionRoles = async () => {
  const listModules = await ModulesModel.find({});
  const listRoles = await RolesModel.find({});
  const lsRolePermission: Array<SchemaPermissionRoles> = [];
  listRoles.forEach(role => {
    let ruleForRole: SchemaPermissionRoles | null;
    listModules.forEach(md => {
      if (role.name === 'admin') {
        ruleForRole = {
          roles: role.id,
          modules: md.id,
          canAdd: true,
          canList: true,
          canSoftDelete: true,
          canDelete: false,
          canView: true,
          canUpdate: true,
        };
      } else if (role.name === 'super_admin') {
        ruleForRole = {
          roles: role.id,
          modules: md.id,
          canAdd: true,
          canList: true,
          canSoftDelete: true,
          canDelete: true,
          canView: true,
          canUpdate: true,
        };
      } else if (role.name === 'sale') {
        ruleForRole = {
          roles: role.id,
          modules: md.id,
          canAdd: md.name === 'orders',
          canUpdate: md.name === 'orders',
          canView: md.name === 'orders' || md.name === 'users',
          canDelete: false,
          canSoftDelete: false,
          canList: md.name === 'orders' || md.name === 'users',
        };
      } else if (role.name === 'store') {
        ruleForRole = {
          roles: role.id,
          modules: md.id,
          canAdd: md.name === 'stock',
          canUpdate: md.name === 'stock',
          canView: md.name === 'stock' || md.name === 'orders',
          canDelete: false,
          canSoftDelete: false,
          canList: md.name === 'stock' || md.name === 'users',
        };
      } else if (role.name === 'member') {
        ruleForRole = {
          roles: role.id,
          modules: md.id,
          canAdd: false,
          canDelete: false,
          canSoftDelete: false,
          canUpdate: false,
          canView: false,
          canList: false,
        };
      }
      if (ruleForRole) {
        lsRolePermission.push(ruleForRole);
      }
    });
  });
  await PermissionModel.insertMany(lsRolePermission).then(() =>
    logger.info('create permission role model success')
  );
};

const createUser = async () => {
  const salt = bcrypt.genSaltSync(10);
  const AdminRole = await RolesModel.findOne({ name: 'admin' });
  const SaRole = await RolesModel.findOne({ name: 'super_admin' });
  const SaleRole = await RolesModel.findOne({ name: 'sale' });
  const StoreRole = await RolesModel.findOne({ name: 'store' });
  const listUser: Array<SchemaUser> = [
    {
      name: 'admin',
      password: await bcrypt.hash('Admin@1234', salt),
      email: 'admin@gmail.com',
      role: [AdminRole.id],
      isEmailVerified: true,
    },
    {
      name: 'super_admin',
      password: await bcrypt.hash('sa@1234', salt),
      email: 'sa@gmail.com',
      role: [SaRole.id],
      isEmailVerified: true,
    },
    {
      name: 'sale',
      password: await bcrypt.hash('sale@1234', salt),
      email: 'sale@gmail.com',
      role: [SaleRole.id],
      isEmailVerified: true,
    },
    {
      name: 'store',
      password: await bcrypt.hash('store@1234', salt),
      email: 'store@gmail.com',
      role: [StoreRole.id],
      isEmailVerified: true,
    },
  ];
  await UserModel.insertMany(listUser).then(() =>
    logger.info('create users model success')
  );
};

const openConnectionDb = () => {
  mongoose
    .connect(configEnv.mongoose.url, configEnv.mongoose.options)
    .then(() => logger.info('connect success'))
    .catch(error => logger.error(error));
};
const migrateUp = async () => {
  await openConnectionDb();
  await createModules();
  await createRoles();
  await createPermissionRoles();
  await createUser();
  if (mongoose.connection.readyState) {
    await mongoose
      .disconnect()
      .then(() => logger.info('close connect success'))
      .catch(error => logger.error(error));
  }
};

export { migrateUp };
