import { Request, Response } from 'express';
import httpStatus from 'http-status';
import rolesServices from '@modules/roles/services';
import usersServices from '@modules/user/services';
import { internalException } from '@utils/exception';
import authServices from './services';
import clientData from './response';
import {
  IRawLogin,
  IRawLogout,
  IRawRefreshToken,
  IRawUser,
  IRequestData,
} from './types/controller';

const register = async (
  req: IRequestData<IRawUser>,
  res: Response
) => {
  try {
    const roleDefault = await rolesServices.getRoleByName('member');
    const dataUser: IRawUser = {
      ...req.body,
      role: [roleDefault.data.id],
    };
    const { error, data } = await usersServices.createUser(dataUser);
    if (error) {
      return res.json(error);
    }
    const tokens = await authServices.generateAuthTokens(data);

    const dataResponseClient = clientData.parseDataRegister(
      data,
      tokens
    );
    res.json(dataResponseClient);
  } catch (err) {
    internalException(res, err);
  }
};

const login = async (req: IRequestData<IRawLogin>, res: Response) => {
  try {
    const { data, error } =
      await authServices.loginUserWithEmailAndPassword(
        req.body.email,
        req.body.password
      );
    if (error) {
      return res.json(error);
    }
    const tokens = await authServices.generateAuthTokens(data);
    const dataResponseClient = clientData.parseDataRegister(
      data,
      tokens
    );
    res.json(dataResponseClient);
  } catch (err) {
    internalException(res, err);
  }
};

const logout = async (
  req: IRequestData<IRawLogout> & Request,
  res: Response
) => {
  try {
    const { refreshToken } = req.body;
    const token =
      (req.headers.authorization &&
        req.headers.authorization.split(' ')[0] === 'Bearer' &&
        req.headers.authorization.split(' ')[1]) ||
      '';
    const { error, data } = await authServices.logout(
      token,
      refreshToken
    );
    if (error) {
      return res.status(httpStatus.OK).json(error);
    }
    res.json(clientData.parseLogout(data));
  } catch (err) {
    internalException(res, err);
  }
};

const refreshToken = async (
  req: IRequestData<IRawRefreshToken>,
  res: Response
) => {
  try {
    const { error, data } = await authServices.refreshAuth(
      req.body.refreshToken
    );
    if (error) {
      return res.json(error);
    }
    const dataResponseClient = clientData.parseRefreshToken(data);
    res.json(dataResponseClient);
  } catch (err) {
    internalException(res, err);
  }
};
const authController = {
  register,
  login,
  logout,
  refreshToken,
};
export default authController;
