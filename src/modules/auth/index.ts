import express from 'express';
import { ROUTE_CONSTANT } from '@routes/constant';
import validate from '@middleware/validateSchema';
import authMiddleWare from '@middleware/auth';
import authController from './controller';
import authValidation from './validation';

const router = express.Router();

router.post(
  ROUTE_CONSTANT.AUTH.REGISTER,
  validate(authValidation.register),
  authController.register
);
router.post(
  ROUTE_CONSTANT.AUTH.LOGIN,
  validate(authValidation.login),
  authController.login
);
router.post(
  ROUTE_CONSTANT.AUTH.LOG_OUT,
  authMiddleWare.authJwt,
  validate(authValidation.logout),
  authController.logout
);
router.post(
  ROUTE_CONSTANT.AUTH.REFRESH_TOKENS,
  validate(authValidation.refreshTokens),
  authController.refreshToken
);

export default router;
