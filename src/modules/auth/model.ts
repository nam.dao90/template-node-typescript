import { Schema, SchemaTypes, model } from 'mongoose';
import {
  SchemaToken,
  TokenModel,
  tokenTypes,
} from '@modules/auth/types/model';

const tokenSchema = new Schema<SchemaToken>(
  {
    token: {
      type: String,
      required: true,
      index: true,
    },
    refreshToken: {
      type: String,
      required: true,
      index: true,
    },
    user: {
      type: SchemaTypes.ObjectId,
      ref: 'User',
      required: true,
    },
    type: {
      type: String,
      enum: [
        tokenTypes.REFRESH,
        tokenTypes.RESET_PASSWORD,
        tokenTypes.VERIFY_EMAIL,
      ],
      required: true,
    },
    expires: {
      type: Date,
      required: true,
    },
    blacklisted: {
      type: Boolean,
      default: false,
    },
  },
  {
    timestamps: true,
  }
);
tokenSchema.statics.isExistToken = async function (
  token
): Promise<boolean> {
  return !!(await this.findOne({
    token,
  }));
};
const Token = model<SchemaToken, TokenModel>('Token', tokenSchema);
export default Token;
