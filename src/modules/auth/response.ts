import { resStatusSuccess } from '@utils/status';
import { IClientResponse } from '@utils/types';
import { IDocumentToken, IDocumentUser } from './types/services';
import { IResRegister, IResToken } from './types/controller';

const parseDataRegister = (
  user: IDocumentUser,
  token: IDocumentToken
): IClientResponse<IResRegister> => {
  return {
    status: resStatusSuccess(),
    data: {
      user: {
        email: user.email,
        isEmailVerified: user.isEmailVerified,
        name: user.name,
        role: user.role &&
          user.role.length > 0 && [user.role[0]?.name],
      },
      token,
    },
  };
};

const parseLogout = (message: string) => ({
  status: resStatusSuccess(),
  data: { message },
});

const parseRefreshToken = (
  token: IDocumentToken
): IClientResponse<IResToken> => {
  return {
    status: resStatusSuccess(),
    data: token,
  };
};
const authData = {
  parseDataRegister,
  parseLogout,
  parseRefreshToken,
};

export default authData;
