import moment, { Moment } from 'moment';
import jwt from 'jsonwebtoken';
import configEnv from '@config/configEnv';
import Token from '@src/modules/auth/model';
import {
  IDocumentToken,
  IDocumentUser,
} from '@src/modules/auth/types/services';
import { IErrorField, IErrorResponse } from '@utils/types';
import { badRequest, handleErrorsModel } from '@utils/apiErrorInput';
import usersServices from '@modules/user/services';
import { tokenTypes } from './types/model';

const generateToken = (
  userId: string,
  expires: Moment,
  type: string,
  secret = configEnv.jwt.secret
) => {
  const payload = {
    sub: userId,
    iat: moment().unix(),
    exp: expires.unix(),
    type,
  };
  return jwt.sign(payload, secret);
};

const saveToken = async (
  token: string,
  refreshToken: string,
  userId: string,
  expires: Moment,
  type: string,
  blacklisted = false
) => {
  const tokenDoc = await Token.create({
    token,
    refreshToken,
    user: userId,
    expires: expires.toDate(),
    type,
    blacklisted,
  });
  return tokenDoc;
};

const generateAuthTokens = async (
  user: IDocumentUser
): Promise<IDocumentToken> => {
  const accessTokenExpires = moment().add(
    configEnv.jwt.accessExpirationMinutes,
    'minutes'
  );
  const accessToken = generateToken(
    user.id,
    accessTokenExpires,
    tokenTypes.ACCESS
  );

  const refreshTokenExpires = moment().add(
    configEnv.jwt.refreshExpirationDays,
    'days'
  );
  const refreshToken = generateToken(
    user.id as string,
    refreshTokenExpires,
    tokenTypes.REFRESH
  );
  await saveToken(
    accessToken,
    refreshToken,
    user.id as string,
    refreshTokenExpires,
    tokenTypes.REFRESH
  );

  return {
    access: {
      token: accessToken,
      expires: accessTokenExpires.toString(),
    },
    refresh: {
      token: refreshToken,
      expires: refreshTokenExpires.toString(),
    },
  };
};

const verifyToken = async (
  refreshToken: string,
  type: 'access' | 'refresh' | 'resetPassword' | 'verifyEmail'
) => {
  const payload = jwt.verify(refreshToken, configEnv.jwt.secret);
  const tokenDoc = await Token.findOne({
    refreshToken,
    type,
    user: payload.sub.toString(),
    blacklisted: false,
  })
    .populate('user')
    .orFail();
  return tokenDoc;
};

const checkExistToken = async (token: string) =>
  Token.isExistToken(token);

const refreshAuth = async (
  refreshToken: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: IDocumentToken | null;
}> => {
  const refreshTokenDoc = await verifyToken(refreshToken, 'refresh');
  const user = refreshTokenDoc?.user as IDocumentUser;
  if (!user) {
    const errorResponse = badRequest('token not found');
    return { error: errorResponse, data: null };
  }
  refreshTokenDoc.remove();
  return {
    error: null,
    data: await generateAuthTokens(user),
  };
};
const loginUserWithEmailAndPassword = async (
  email: string,
  password: string
): Promise<{
  error: IErrorResponse<IErrorField[]> | null;
  data: IDocumentUser | null;
}> => {
  const user = await usersServices.getUserViaEmail(email);
  if (!user || !(await user.isPasswordMatch(password))) {
    const errorResponse = handleErrorsModel({
      key: 'email',
      message: 'Incorrect email or password',
    });
    return { error: errorResponse, data: null };
  }
  return { error: null, data: user };
};
const logout = async (
  token: string,
  refreshToken: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: string | null;
}> => {
  const tokenUser = await Token.findOne({
    token,
    refreshToken,
    type: 'refresh',
    blacklisted: false,
  });
  if (!tokenUser) {
    const errorResponse = badRequest('token not found');
    return { error: errorResponse, data: null };
  }
  tokenUser.remove();
  return { error: null, data: 'logout success' };
};
const authServices = {
  generateAuthTokens,
  verifyToken,
  refreshAuth,
  loginUserWithEmailAndPassword,
  logout,
  checkExistToken,
};
export default authServices;
