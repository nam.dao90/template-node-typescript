export interface IRawUser {
  name: string;
  email: string;
  password: string;
  role?: string[];
}
export interface IRawLogin {
  email: string;
  password: string;
}
export interface IRawLogout {
  refreshToken: string;
}
export type IRawRefreshToken = IRawLogout;
export type IRequestData<T> = {
  body: T;
};
export type IRequestParams<T> = {
  params: T;
};
export type IResToken = {
  access: {
    token: string;
    expires: string;
  };
  refresh: {
    token: string;
    expires: string;
  };
};
export type IResRegister = {
  user: {
    name: string;
    email: string;
    role: string[];
    isEmailVerified: boolean;
  };
  token: IResToken;
};

export type IRawRole = { name: string; aliasName: string };
export type IRawBodyRole = { aliasName: string };
export type IRawParamsRole = { roleName: string };

export type IRawModule = IRawRole;
export type IRawBodyModule = IRawBodyRole;
export type IRawParamsModule = { moduleName: string };

export type IPermissionRole = {
  canAdd: boolean;
  canDelete: boolean;
  canSoftDelete: boolean;
  canUpdate: boolean;
  canView: boolean;
  canList: boolean;
  moduleName: string;
};
export type IResPermissionList = {
  role: string;
  permissionRole: IPermissionRole[];
};
