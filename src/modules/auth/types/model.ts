import { SchemaUser } from '@modules/user/types/model';
import { Model, PopulatedDoc, Document } from 'mongoose';

export const tokenTypes = {
  ACCESS: 'access',
  REFRESH: 'refresh',
  RESET_PASSWORD: 'resetPassword',
  VERIFY_EMAIL: 'verifyEmail',
};
export interface SchemaToken extends Document {
  user: PopulatedDoc<SchemaUser>;
  blacklisted: boolean;
  token: string;
  refreshToken: string;
  type: 'access' | 'refresh' | 'resetPassword' | 'verifyEmail';
  expires: Date;
}
export type TokenModel = Model<SchemaToken, { id: string }> & {
  isExistToken(token: string): boolean;
};
