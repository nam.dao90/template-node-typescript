import { LeanDocument } from 'mongoose';
import {
  InstanceUserMethods,
  SchemaUser,
} from '@modules/user/types/model';

export type IDocumentUser = LeanDocument<SchemaUser> &
  InstanceUserMethods;
export type IDocumentToken = {
  access: {
    token: string;
    expires: string;
  };
  refresh: {
    token: string;
    expires: string;
  };
};
