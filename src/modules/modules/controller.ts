import { Request, Response } from 'express';
import AbilityApp from '@middleware/acl/ability';
import permissionServices from '@src/modules/permission/services';
import { internalException } from '@utils/exception';
import { IRequestData } from '@utils/types';
import modulesServices from './services';
import {
  IRawModule,
  IRawParamsModule,
  IRawBodyModule,
} from './types/controller';
import modulesResponse from './response';

const createModules = async (
  req: IRequestData<IRawModule>,
  res: Response
) => {
  try {
    const { data, error } = await modulesServices.createModule(
      req.body
    );
    if (error) {
      return res.json(error);
    }
    await permissionServices.addPermissionWithModule(data);
    AbilityApp.getInstance().clearPermissionRole();
    const resRole = modulesResponse.parseModules(data);
    return res.json(resRole);
  } catch (err) {
    internalException(res, err);
  }
};

const listModules = async (_req: Request, res: Response) => {
  try {
    const lsRole = await modulesServices.getListModules();
    const parsListRole = modulesResponse.parseListModules(lsRole);
    return res.json(parsListRole);
  } catch (err) {
    internalException(res, err);
  }
};

const getModuleInfo = async (
  req: Request<IRawParamsModule>,
  res: Response
) => {
  try {
    const { data, error } = await modulesServices.getModuleByName(
      req.params.moduleName
    );
    if (error) {
      return res.json(error);
    }
    const resRole = modulesResponse.parseModules(data);
    return res.json(resRole);
  } catch (err) {
    internalException(res, err);
  }
};
const updateModule = async (
  req: Request<
    IRawParamsModule,
    Record<string, string>,
    IRawBodyModule
  >,
  res: Response
) => {
  try {
    const { data, error } = await modulesServices.updateAliasModule(
      req.params.moduleName,
      req.body.aliasName
    );
    if (error) {
      return res.json(error);
    }
    const resRole = modulesResponse.parseModules(data);
    return res.json(resRole);
  } catch (err) {
    internalException(res, err);
  }
};
const modulesController = {
  createModules,
  listModules,
  updateModule,
  getModuleInfo,
};
export default modulesController;
