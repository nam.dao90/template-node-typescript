import express from 'express';
import roles from '@middleware/roles';
import validate from '@middleware/validateSchema';
import authMiddleWare from '@middleware/auth';
import { ROUTE_CONSTANT } from '@routes/constant';
import moduleController from './controller';
import moduleValidation from './validation';

const router = express.Router();
router.use(authMiddleWare.authToken);
router
  .route(ROUTE_CONSTANT.MODULES.DEFAULT)
  .get(
    authMiddleWare.authJwt,
    roles.accessRoleByModule('canList', 'modules'),
    moduleController.listModules
  )
  .post(
    authMiddleWare.authJwt,
    validate(moduleValidation.create),
    roles.accessRoleByModule('canAdd', 'modules'),
    moduleController.createModules
  );

router
  .route(ROUTE_CONSTANT.MODULES.DETAIL)
  .get(
    authMiddleWare.authJwt,
    roles.accessRoleByModule('canView', 'modules'),
    moduleController.getModuleInfo
  )
  .put(
    authMiddleWare.authJwt,
    validate(moduleValidation.update),
    roles.accessRoleByModule('canUpdate', 'modules'),
    moduleController.updateModule
  );

export default router;
