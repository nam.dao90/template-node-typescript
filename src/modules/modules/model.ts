import { Schema, model } from 'mongoose';
import { SchemaModules, ModulesModel } from './types/model';

const moduleSchema = new Schema<SchemaModules>(
  {
    name: {
      type: String,
      required: true,
      unique: true,
      index: true,
    },
    aliasName: {
      type: String,
      required: true,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  {
    timestamps: true,
  }
);

moduleSchema.statics.isModuleTaken = async function (
  name
): Promise<boolean> {
  const module = await this.findOne({
    name,
    deletedAt: null,
  });
  return !!module;
};
const Modules = model<SchemaModules, ModulesModel>(
  'Modules',
  moduleSchema
);
export default Modules;
