import { resStatusSuccess } from '@utils/status';
import { IClientResponse } from '@utils/types';
import { IDocumentModule } from './types/services';

const parseModules = (
  module: IDocumentModule
): IClientResponse<{ module: string; alias: string }> => {
  return {
    status: resStatusSuccess(),
    data: { module: module.name, alias: module.aliasName },
  };
};

const parseListModules = (
  lsDocumentModules: IDocumentModule[]
): IClientResponse<Array<{ alias: string; module: string }>> => {
  const parsModule = lsDocumentModules.map(m => {
    return {
      alias: m.aliasName,
      module: m.name,
    };
  });
  return {
    status: resStatusSuccess(),
    data: parsModule,
  };
};

const modulesResponse = {
  parseModules,
  parseListModules,
};
export default modulesResponse;
