import Modules from '@modules/modules/model';
import { badRequest, handleErrorsModel } from '@utils/apiErrorInput';
import { IErrorField, IErrorResponse } from '@utils/types';
import { IDocumentRoles } from '@modules/roles/types/services';
import { IRawModule } from './types/controller';
import { IDocumentModule } from './types/services';

const getListModules = async () => {
  return Modules.find({ deletedAt: null }).lean();
};

const createModule = async (
  moduleBody: IRawModule
): Promise<{
  error: null | IErrorResponse<IErrorField[]>;
  data: IDocumentRoles;
}> => {
  if (await Modules.isModuleTaken(moduleBody.name)) {
    const errorResponse = handleErrorsModel({
      key: 'module',
      message: 'Module is taken',
    });
    return { error: errorResponse, data: null };
  }
  return { error: null, data: await Modules.create(moduleBody) };
};

const getModuleByName = async (
  name: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: IDocumentModule | null;
}> => {
  const existRole = await Modules.findOne({
    name,
    deletedAt: null,
  });
  if (!existRole) {
    const errorResponse = badRequest('modules not found');
    return { error: errorResponse, data: null };
  }
  return { error: null, data: existRole };
};
const updateAliasModule = async (
  name: string,
  aliasName: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: IDocumentRoles | null;
}> => {
  const existModules = await Modules.findOneAndUpdate(
    { name },
    { aliasName },
    { lean: true, new: true }
  );
  if (!existModules) {
    const errorResponse = badRequest('Module not found');
    return { error: errorResponse, data: null };
  }
  return { error: null, data: existModules };
};
const modulesServices = {
  getListModules,
  createModule,
  getModuleByName,
  updateAliasModule,
};
export default modulesServices;
