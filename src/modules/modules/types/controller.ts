export type IRawModule = { name: string; aliasName: string };
export type IRawBodyModule = { aliasName: string };
export type IRawParamsModule = { moduleName: string };
