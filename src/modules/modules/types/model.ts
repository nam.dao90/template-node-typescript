import { Model } from 'mongoose';

export type SchemaModules = {
  name: string;
  aliasName: string;
  deletedAt?: string | null;
  createdAt?: string;
  updatedAt?: string;
};
export type ModulesModel = Model<SchemaModules> & {
  isModuleTaken(name: string): boolean;
};
