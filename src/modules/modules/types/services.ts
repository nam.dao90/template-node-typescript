import { Document, LeanDocument } from 'mongoose';
import { SchemaModules } from './model';

export type IDocumentModule = LeanDocument<
  SchemaModules & Document<SchemaModules>
>;
