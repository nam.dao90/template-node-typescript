import Joi from 'joi';

const update = {
  body: Joi.object().keys({
    aliasName: Joi.string().required(),
  }),
};
const create = {
  body: Joi.object().keys({
    name: Joi.string().required(),
    aliasName: Joi.string().required(),
  }),
};
const modulesValidation = {
  update,
  create,
};
export default modulesValidation;
