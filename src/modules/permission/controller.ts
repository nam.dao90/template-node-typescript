import permissionServices from '@modules/permission/services';
import { internalException } from '@utils/exception';
import { Request, Response } from 'express';
import permissionResponse from './response';

const listPermission = async (_req: Request, res: Response) => {
  try {
    const { data, error } =
      await permissionServices.getPermissionGroup();
    if (error) {
      return res.json(error);
    }
    const parsePermission =
      permissionResponse.parseListPermission(data);
    return res.json(parsePermission);
  } catch (err) {
    internalException(res, err);
  }
};

const permissionController = {
  listPermission,
};
export default permissionController;
