import express from 'express';
import authMiddleWare from '@middleware/auth';
import roles from '@middleware/roles';
import { ROUTE_CONSTANT } from '@routes/constant';
import permissionController from './controller';

const router = express.Router();
router.use(authMiddleWare.authToken);

router
  .route(ROUTE_CONSTANT.PERMISSION.DEFAULT)
  .get(
    authMiddleWare.authJwt,
    roles.accessRolesBase('canList'),
    permissionController.listPermission
  );

export default router;
