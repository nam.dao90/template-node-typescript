import { Schema, model, SchemaTypes } from 'mongoose';
import {
  SchemaPermissionRoles,
  PermissionRolesModel,
} from './types/model';

const moduleSchema = new Schema<SchemaPermissionRoles>(
  {
    roles: {
      type: SchemaTypes.ObjectId,
      ref: 'Roles',
      required: true,
    },
    modules: {
      type: SchemaTypes.ObjectId,
      ref: 'Modules',
      required: true,
    },
    canAdd: {
      type: Boolean,
      default: false,
      require: true,
    },
    canView: {
      type: Boolean,
      default: false,
      require: true,
    },
    canUpdate: {
      type: Boolean,
      default: false,
      require: true,
    },
    canList: {
      type: Boolean,
      default: false,
      require: true,
    },
    canSoftDelete: {
      type: Boolean,
      default: false,
      require: true,
    },
    canDelete: {
      type: Boolean,
      default: false,
      require: true,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  {
    timestamps: true,
  }
);

const PermissionRoles = model<
  SchemaPermissionRoles,
  PermissionRolesModel
>('PermissionRoles', moduleSchema);
export default PermissionRoles;
