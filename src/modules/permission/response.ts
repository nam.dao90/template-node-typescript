import { resStatusSuccess } from '@utils/status';
import { IClientResponse } from '@utils/types';
import groupBy from 'lodash/groupBy';
import { IDocumentPermission } from './types/services';
import {
  IPermissionRole,
  IResPermissionList,
} from './types/controller';

const parseListPermission = (
  lsDocumentPermissions: IDocumentPermission[]
): IClientResponse<Array<IResPermissionList>> => {
  const dataGroup = Object.entries(
    groupBy(lsDocumentPermissions, 'roles.name')
  );

  const listParsePermission = dataGroup.map(group => {
    const role = group[0];
    const permissionRole: IPermissionRole[] = group[1].map(ls => {
      return {
        canAdd: ls.canAdd,
        canDelete: ls.canDelete,
        canSoftDelete: ls.canSoftDelete,
        canUpdate: ls.canUpdate,
        canView: ls.canView,
        canList: ls.canList,
        moduleName: ls.modules.name,
      };
    });
    return {
      role,
      permissionRole,
    };
  });
  return {
    status: resStatusSuccess(),
    data: listParsePermission,
  };
};

const permissionResponse = {
  parseListPermission,
};
export default permissionResponse;
