import Modules from '@modules/modules/model';
import Roles from '@modules/roles/model';
import { badRequest } from '@utils/apiErrorInput';
import { IDocumentRoles } from '@modules/roles/types/services';
import { IDocumentModule } from '@modules/modules/types/services';
import { IDocumentPermission } from './types/services';
import PermissionRoles from './model';
import { SchemaPermissionRoles } from './types/model';

const getPermissionRole = async (): Promise<
  IDocumentPermission[]
> => {
  const listPermission = await PermissionRoles.find({})
    .populate({ path: 'modules', model: Modules })
    .populate({ path: 'roles', model: Roles })
    .orFail();
  return listPermission;
};

const getPermissionGroup = async (): Promise<{
  error: any;
  data: any;
}> => {
  const listPermission = await PermissionRoles.find({})
    .select({
      _id: 0,
      canView: 1,
      canUpdate: 1,
      canAdd: 1,
      canSoftDelete: 1,
      canList: 1,
      canDelete: 1,
      roles: 1,
      modules: 1,
    })
    .populate({ path: 'modules', model: Modules, select: 'name' })
    .populate({ path: 'roles', model: Roles, select: 'name' })
    .lean();
  if (!listPermission) {
    return {
      error: badRequest('list permission not found'),
      data: null,
    };
  }
  return { error: null, data: listPermission };
};

const addPermissionWithModule = async (
  newModule: IDocumentModule
) => {
  const listRoles = await Roles.find({ deletedAt: null });
  const newPermissionModule: Array<SchemaPermissionRoles> = [];
  listRoles.forEach(role => {
    const ruleForRole = {
      canAdd: role.name === 'admin' || role.name === 'super_admin',
      canDelete: role.name === 'super_admin',
      canList: role.name === 'admin' || role.name === 'super_admin',
      canSoftDelete:
        role.name === 'admin' || role.name === 'super_admin',
      canUpdate: role.name === 'admin' || role.name === 'super_admin',
      canView: role.name === 'admin' || role.name === 'super_admin',
      roles: role.id,
      modules: newModule.id,
    };
    newPermissionModule.push(ruleForRole);
  });
  return PermissionRoles.insertMany(newPermissionModule);
};

const addPermissionWithRole = async (newRole: IDocumentRoles) => {
  const listModules = await Modules.find({ deletedAt: null });
  const newPermissionRole: Array<SchemaPermissionRoles> = [];
  listModules.forEach(module => {
    const ruleForRole = {
      canAdd: false,
      canDelete: false,
      canList: false,
      canSoftDelete: false,
      canUpdate: false,
      canView: false,
      roles: newRole.id,
      modules: module.id,
    };
    newPermissionRole.push(ruleForRole);
  });
  return PermissionRoles.insertMany(newPermissionRole);
};
const permissionServices = {
  getPermissionRole,
  getPermissionGroup,
  addPermissionWithModule,
  addPermissionWithRole,
};
export default permissionServices;
