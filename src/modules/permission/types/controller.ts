export type IPermissionRole = {
  canAdd: boolean;
  canDelete: boolean;
  canSoftDelete: boolean;
  canUpdate: boolean;
  canView: boolean;
  canList: boolean;
  moduleName: string;
};
export type IResPermissionList = {
  role: string;
  permissionRole: IPermissionRole[];
};
