import { LeanDocument, Model } from 'mongoose';
import { SchemaModules } from '@modules/modules/types/model';
import { SchemaRoles } from '@modules/roles/types/model';

export type SchemaPermissionRoles = {
  roles: LeanDocument<SchemaRoles>;
  modules: LeanDocument<SchemaModules>;
  canAdd: boolean;
  canView: boolean;
  canUpdate: boolean;
  canSoftDelete: boolean;
  canDelete: boolean;
  canList: boolean;
};
export type PermissionRolesModel = Model<SchemaPermissionRoles> & {
  isModulesTaken(name: string): boolean;
};
