import { LeanDocument } from 'mongoose';
import { SchemaPermissionRoles } from './model';

export type IDocumentPermission = LeanDocument<SchemaPermissionRoles>;
