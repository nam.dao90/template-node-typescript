import { Request, Response } from 'express';
import AbilityApp from '@middleware/acl/ability';
import permissionServices from '@modules/permission/services';
import { internalException } from '@utils/exception';
import { IRequestData } from '@utils/types';
import rolesServices from './services';
import {
  IRawRole,
  IRawParamsRole,
  IRawBodyRole,
} from './types/controller';
import rolesResponse from './response';

const createRole = async (
  req: IRequestData<IRawRole>,
  res: Response
) => {
  try {
    const { data, error } = await rolesServices.createRole(req.body);
    if (error) {
      return res.json(error);
    }
    await permissionServices.addPermissionWithRole(data);
    AbilityApp.getInstance().clearPermissionRole();
    const resRole = rolesResponse.parseRoles(data);
    return res.json(resRole);
  } catch (err) {
    internalException(res, err);
  }
};

const listRole = async (_req: Request, res: Response) => {
  try {
    const lsRole = await rolesServices.getListRole();
    const parsListRole = rolesResponse.parseListRoles(lsRole);
    return res.json(parsListRole);
  } catch (err) {
    internalException(res, err);
  }
};

const getRoleInfo = async (
  req: Request<IRawParamsRole>,
  res: Response
) => {
  try {
    const { data, error } = await rolesServices.getRoleByName(
      req.params.roleName
    );
    if (error) {
      return res.json(error);
    }
    const resRole = rolesResponse.parseRoles(data);
    return res.json(resRole);
  } catch (err) {
    internalException(res, err);
  }
};
const updateRole = async (
  req: Request<IRawParamsRole, Record<string, string>, IRawBodyRole>,
  res: Response
) => {
  try {
    const { data, error } = await rolesServices.updateAliasRole(
      req.params.roleName,
      req.body.aliasName
    );
    if (error) {
      return res.json(error);
    }
    const resRole = rolesResponse.parseRoles(data);
    return res.json(resRole);
  } catch (err) {
    internalException(res, err);
  }
};
const roleController = {
  createRole,
  listRole,
  updateRole,
  getRoleInfo,
};
export default roleController;
