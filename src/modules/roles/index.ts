import express from 'express';
import roles from '@middleware/roles';
import validate from '@middleware/validateSchema';
import authMiddleWare from '@middleware/auth';
import { ROUTE_CONSTANT } from '@routes/constant';
import roleController from './controller';
import roleValidation from './validation';

const router = express.Router();
router.use(authMiddleWare.authToken);
router
  .route(ROUTE_CONSTANT.ROLES.DEFAULT)
  .get(
    authMiddleWare.authJwt,
    roles.accessRoleByModule('canList', 'roles'),
    roleController.listRole
  )
  .post(
    authMiddleWare.authJwt,
    validate(roleValidation.create),
    roles.accessRoleByModule('canAdd', 'roles'),
    roleController.createRole
  );

router
  .route(ROUTE_CONSTANT.ROLES.DETAIL)
  .get(
    authMiddleWare.authJwt,
    roles.accessRoleByModule('canView', 'roles'),
    roleController.getRoleInfo
  )
  .put(
    authMiddleWare.authJwt,
    validate(roleValidation.update),
    roles.accessRoleByModule('canUpdate', 'roles'),
    roleController.updateRole
  );

export default router;
