import { Schema, model } from 'mongoose';
import { SchemaRoles, RolesModel } from './types/model';

const rolesSchema = new Schema<SchemaRoles>(
  {
    name: {
      type: String,
      required: true,
      unique: true,
      index: true,
    },
    aliasName: {
      type: String,
      required: true,
    },
    deletedAt: {
      type: Date,
      default: null,
    },
  },
  {
    timestamps: true,
  }
);

rolesSchema.statics.isRoleTaken = async function (
  name
): Promise<boolean> {
  const roles = await this.findOne({
    name,
    deletedAt: null,
  });
  return !!roles;
};
const Roles = model<SchemaRoles, RolesModel>('Roles', rolesSchema);
export default Roles;
