import { resStatusSuccess } from '@utils/status';
import { IClientResponse } from '@utils/types';
import { IDocumentRoles } from './types/services';

const parseRoles = (
  role: IDocumentRoles
): IClientResponse<{ role: string; alias: string }> => {
  return {
    status: resStatusSuccess(),
    data: { role: role.name, alias: role.aliasName },
  };
};

const parseListRoles = (
  lsDocumentRoles: IDocumentRoles[]
): IClientResponse<Array<{ alias: string; role: string }>> => {
  const parsRole = lsDocumentRoles.map(r => {
    return {
      alias: r.aliasName,
      role: r.name,
    };
  });
  return {
    status: resStatusSuccess(),
    data: parsRole,
  };
};

const rolesResponse = {
  parseRoles,
  parseListRoles,
};
export default rolesResponse;
