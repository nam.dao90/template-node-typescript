import Roles from '@modules/roles/model';
import { badRequest, handleErrorsModel } from '@utils/apiErrorInput';
import { IErrorField, IErrorResponse } from '@utils/types';
import { IRawRole } from './types/controller';
import { IDocumentRoles } from './types/services';

const getListRole = async () => {
  return Roles.find({ deletedAt: null }).lean();
};

const createRole = async (
  roleBody: IRawRole
): Promise<{
  error: null | IErrorResponse<IErrorField[]>;
  data: IDocumentRoles;
}> => {
  if (await Roles.isRoleTaken(roleBody.name)) {
    const errorResponse = handleErrorsModel({
      key: 'role',
      message: 'Role is taken',
    });
    return { error: errorResponse, data: null };
  }
  return { error: null, data: await Roles.create(roleBody) };
};

const getRoleByName = async (
  name: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: IDocumentRoles | null;
}> => {
  try {
    const existRole = await Roles.findOne({ name, deletedAt: null });
    if (!existRole) {
      const errorResponse = badRequest('role not found');
      return { error: errorResponse, data: null };
    }
    return { error: null, data: existRole };
  } catch (err) {
    const errorResponse = badRequest('role not found');
    return { error: errorResponse, data: null };
  }
};

const updateAliasRole = async (
  name: string,
  aliasName: string
): Promise<{
  error: IErrorResponse<{ message: string | number }> | null;
  data: IDocumentRoles | null;
}> => {
  const existRole = await Roles.findOneAndUpdate(
    { name },
    { aliasName },
    { lean: true, new: true }
  );
  if (!existRole) {
    const errorResponse = badRequest('role not found');
    return { error: errorResponse, data: null };
  }
  return { error: null, data: existRole };
};
const rolesServices = {
  getRoleByName,
  getListRole,
  createRole,
  updateAliasRole,
};
export default rolesServices;
