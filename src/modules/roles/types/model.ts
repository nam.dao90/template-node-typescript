import { Model } from 'mongoose';

export type SchemaRoles = {
  name: string;
  aliasName: string;
  deletedAt?: string | null;
  createdAt?: string;
  updatedAt?: string;
};
export type RolesModel = Model<SchemaRoles> & {
  isRoleTaken(name: string): boolean;
};
