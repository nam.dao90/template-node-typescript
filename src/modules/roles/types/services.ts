import { Document, LeanDocument } from 'mongoose';
import { SchemaRoles } from './model';

export type IDocumentRoles = LeanDocument<
  SchemaRoles & Document<SchemaRoles>
>;
