import { Schema, model, SchemaTypes } from 'mongoose';
import validator from 'validator';
import bcrypt from 'bcrypt';
import { SchemaUser, UserModel } from '@modules/user/types/model';

const userSchema = new Schema<SchemaUser>(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value: string) {
        if (!validator.isEmail(value)) {
          throw new Error('Invalid email');
        }
      },
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 8,
      validate(value: string) {
        if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
          throw new Error(
            'Password must contain at least one letter and one number'
          );
        }
      },
      private: true, // used by the toJSON plugin
    },
    role: [
      {
        type: SchemaTypes.ObjectId,
        ref: 'Roles',
        required: true,
      },
    ],
    isEmailVerified: {
      type: Boolean,
      default: false,
    },
    deletedAt: {
      type: String,
      default: null,
    },
  },
  {
    timestamps: true,
  }
);

userSchema.statics.isEmailTaken = async function (
  email,
  excludeUserId
): Promise<boolean> {
  const user = await this.findOne({
    email,
    _id: { $ne: excludeUserId },
  });
  return !!user;
};

userSchema.methods.isPasswordMatch = async function (
  password
): Promise<boolean> {
  return bcrypt.compare(password, this.password);
};

userSchema.pre('save', async function (next) {
  // const user = this;
  if (this.isModified('password')) {
    const salt = bcrypt.genSaltSync(10);
    this.password = await bcrypt.hash(this.password, salt);
  }
  next();
});

const User = model<SchemaUser, UserModel>('User', userSchema);
export default User;
