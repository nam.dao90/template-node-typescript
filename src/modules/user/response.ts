import { resStatusSuccess } from '@utils/status';
import { IClientResponse } from '@utils/types';
import { IDocumentUser } from './types/service';
import { IResUserList } from './types/controller';

const parseUserList = (
  users: Array<IDocumentUser>
): IClientResponse<IResUserList> => {
  const lsUser: IResUserList = [];
  users.forEach(us => {
    const user = {
      name: us.name,
      email: us.email,
      isEmailVerified: us.isEmailVerified,
    };
    lsUser.push(user);
  });
  return {
    status: resStatusSuccess(),
    data: lsUser,
  };
};

const userResponse = {
  parseUserList,
};

export default userResponse;
