import User from '@modules/user/model';
import { handleErrorsModel } from '@utils/apiErrorInput';
import { IErrorField, IErrorResponse } from '@utils/types';
import { IRawUser } from './types/controller';
import { IDocumentUser } from './types/service';

const createUser = async (
  userBody: IRawUser
): Promise<{
  error: IErrorResponse<IErrorField[]> | null;
  data: IDocumentUser | null;
}> => {
  if (await User.isEmailTaken(userBody.email)) {
    const errorResponse = handleErrorsModel({
      key: 'email',
      message: 'Email already taken',
    });
    return { error: errorResponse, data: null };
  }
  const dataUser = await User.create(userBody);
  const dataUserRole = await User.populate(dataUser, {
    path: 'role',
  });
  return { error: null, data: dataUserRole };
};

const getUserViaEmail = (email: string) => User.findOne({ email });
const getUserById = (id: string) => User.findById(id);
const queryUsers = () => User.find();

const usersServices = {
  createUser,
  getUserViaEmail,
  getUserById,
  queryUsers,
};

export default usersServices;
