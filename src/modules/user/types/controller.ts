export type IResUserList = Array<{
  name: string;
  email: string;
  isEmailVerified: boolean;
}>;

export interface IRawUser {
  name: string;
  email: string;
  password: string;
  role?: string[];
}
