import { SchemaRoles } from '@modules/roles/types/model';
import { LeanDocument, Model } from 'mongoose';

export type SchemaUser = {
  id?: string;
  name: string;
  email: string;
  password: string;
  role: Array<LeanDocument<SchemaRoles>>;
  isEmailVerified?: boolean;
};

export type InstanceUserMethods = {
  isPasswordMatch(password: string): boolean;
};
export interface UserModel
  extends Model<
    SchemaUser,
    Record<string, string>,
    InstanceUserMethods
  > {
  isEmailTaken(email: string, excludeUserId?: string): boolean;
}
