import { LeanDocument } from 'mongoose';
import { InstanceUserMethods, SchemaUser } from './model';

export type IDocumentUser = LeanDocument<SchemaUser> &
  InstanceUserMethods;
