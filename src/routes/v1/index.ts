import express from 'express';
import authRoute from '@modules/auth';
import usersRoute from '@modules/user';
import rolesRoute from '@modules/roles';
import modulesRoute from '@modules/modules';
import permissionRoute from '@modules/permission';
import { ROUTE_GROUP } from '../constant';

const routerApi = express.Router();

const defaultRoutes = [
  {
    path: ROUTE_GROUP.AUTH,
    route: authRoute,
  },
  {
    path: ROUTE_GROUP.USERS,
    route: usersRoute,
  },
  {
    path: ROUTE_GROUP.ROLES,
    route: rolesRoute,
  },
  {
    path: ROUTE_GROUP.MODULES,
    route: modulesRoute,
  },
  {
    path: ROUTE_GROUP.PERMISSION,
    route: permissionRoute,
  },
];

defaultRoutes.forEach(route => {
  routerApi.use(route.path, route.route);
});

export default routerApi;
